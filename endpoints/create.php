<?php
//This works for ALL tables, not case sensitive, must use column names as they appear in the tables for query string parameters
//example:
//http://ec2-3-91-152-108.compute-1.amazonaws.com/endpoints/create.php?target=BICYCLE&serialnumber=13&customerid=1&modeltype=ExtremeBike&paintid=1&listprice=349&storeid=1
//example 2:
//http://ec2-3-91-152-108.compute-1.amazonaws.com/endpoints/create.php?target=RETAILSTORE&STOREID=5&STORENAME=TheBestBikeShop&PHONE=4234561234&ADDRESS=123Someplace&ZIPCODE=55443&CITYID=2

include('includes/config.php');
$table = $_GET['target'];

        parse_str($_SERVER['QUERY_STRING'], $data);
        array_shift($data);

        $cols = implode(", ", array_keys($data));
        $vals =  implode("', '", array_values($data));

        $sql = "INSERT INTO BIKE_SHOP.$table($cols) VALUES ('$vals')";
        $status = $pdo->prepare($sql)->execute();

        if($status)
		echo "Successfully inserted data into $table: " . json_encode($data);
        else
		echo "Failed to insert data into $table: " . json_encode($data);


//BICYCLE table columns
//SERIALNUMBER, CUSTOMERID, MODELTYPE, PAINTID, LISTPRICE, STOREID


//RETAILSTORE table columns
//STOREID, STORENAME, PHONE, ADDRESS, ZIPCODE, CITYID


//EMPLOYEE table columns
//EMPLOYEEID, TAXPAYERID, LASTNAME, FIRSTNAME, HOMEPHONE, ADDRESS, ZIPCODE, CITYID, DATEHIRED, DATERELEASED, CURRENTMANAGER, SALARYGRADE, SALARY, TITLE, WORKAREA


//CUSTOMER table columns
//CUSTOMERID, PHONE, FIRSTNAME, LASTNAME, ADDRESS, ZIPCODE, CITYID, BALANCEDUE

?>
