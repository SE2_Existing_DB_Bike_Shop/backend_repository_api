<?php

class LogMetrics {

        public function random_hex() {
                $hex = dechex(rand(0,4294967295)) . dechex(rand(0,4294967295)) . dechex(rand(0,4294967295));
                $hex = str_pad($hex, 24, '0', STR_PAD_LEFT);
                return $hex;
        }

        public function log_read_bike()
        {
                $project_id = '5de6ed09072b6f0a2c386bcb';
                $read_category_id = '111111111111111111111111';
                $base_url = 'http://metricapi-dev.us-west-2.elasticbeanstalk.com/api/';
                $t = time();
                date_default_timezone_set('EST');
                $date = date('c', $t);
                $hex = $this->random_hex();
                echo('Generated hex: ' . $hex);
                $curl_string = 'curl -X POST "' . $base_url . 'Entry" -H "accept: text/plain" -H "Content-Type: application/json-patch+json" -d "{ \"id\": \"' . $hex . '\", \"timestamp\": \"' . $date . '\", \"category\": ' . $read_category_id . '\", \"value\": \"1\"}"';
                exec($curl_string);
        }

}
?>
